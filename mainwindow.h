#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QElapsedTimer>
#include <QThread>
#include <QMetaType>
#include <QLabel>
#include <QMutex>

#include <maciSimpleClient.h>
#include <loggingLoggingProxy.h>
#include <loggingLogSvcHandler.h>
#include <loggingMACROS.h>
#include <ControlBasicInterfacesC.h>

#include <acscomponentImpl.h>
#include "OpticalTelescopeBaseS.h"
#include "OpticalTelescopeBaseC.h"
#include "OpticalTelescopeS.h"
#include "OpticalTelescopeC.h"

#include "ControlAntennaInterfacesC.h"
#include <ControlCommonCallbacksC.h>
#include <AntennaC.h>
#include <ControlBasicInterfacesC.h>

namespace Ui {
    class MainWindow;
}


class ExposureWorker;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setClient(maci::SimpleClient *);

private:
    Ui::MainWindow *ui;
    maci::SimpleClient * client;
    maci::SmartPtr<Control::OpticalTelescope> opt;
    QThread corbaRefreshThread;
    QThread exposureWorkerThread;
    ExposureWorker * exposureWorker;
    QLabel previewPanel;
    void previewFitsFile(QString filename);
    void startExposure();
    QMutex * exposuremutex;

private slots:
    void on_log_msg(QString &msg);
    void on_checkBox_acs_preview_stateChanged(int state);
    void on_pushButton_focuser_autoconnect_clicked();
    void on_comboBox_focuser_speed_currentIndexChanged(int index);
    void on_pushButton_focuser_target_set_clicked();
    void on_pushButton_focuser_current_set_clicked();
    void on_pushButton_focuser_abort_clicked();
    void on_pushButton_focuser_disconnect_clicked();
    void on_pushButton_focuser_connect_clicked();
    void on_pushButton_hwOperational_clicked();
    void on_pushButton_hwInitialize_clicked();
    void on_pushButton_hwConfigure_clicked();
    void on_pushButton_hwStart_clicked();
    void on_pushButton_hwStop_clicked();
    void updateCorbaDataInUI(const struct refreshdata&);
    void on_refresh_error(const QString&);
    void on_checkBox_focuser_halfstepping_stateChanged(int state);
    void on_pushButton_focuser_init_temp_clicked();
    void on_pushButton_sbig_connect_clicked();
    void on_pushButton_sbig_disconnect_clicked();
    void on_checkBox_sbig_cooler_stateChanged(int state);
    void on_doubleSpinBox_cooler_setpoint_valueChanged(double val);
    void on_doubleSpinBox_exposure_valueChanged(double val);
    void on_doubleSpinBox_sbig_partial_valueChanged(double val);
    void on_comboBox_sbig_quality_currentIndexChanged(int);
    void on_comboBox_sbig_type_currentIndexChanged(int);
    void on_pushButton_focuser_autohome_clicked();
    void on_pushButton_clearlog_clicked();
    void on_pushButton_exposure_start_clicked();
    void on_pushButton_exposure_abort_clicked();
    void on_lineEdit_ds9_host_editingFinished();
    void on_spinBox_ds9_port_editingFinished();
    void on_groupBox_ds9_output_clicked(bool);
    void on_exposure_done();
    void on_exposure_progress(const int progress);
    void on_filesizeKnown(int);
    void on_transfer_done();
    void on_transfer_progress(int);
    void on_pushButton_sbig_reset_clicked();
};

struct refreshdata
{
    Control::HardwareDevice::HwState optstate;
    Control::HardwareDevice::HwState mountstate;

    CORBA::Boolean focuser_connected;
    CORBA::Boolean sbig_connected;

    // focuser related data:
    CORBA::Boolean ismoving;
    CORBA::Long    position;
    CORBA::Long    target;
    CORBA::Boolean ishalfstep;
    CORBA::Long    temp;
    CORBA::Long    speed;

    // sbig related data:
    CORBA::Boolean exposureinprogress;
    CORBA::Double actualtemp;
    CORBA::Double targettemp;
    CORBA::Double exposuretime;
    CORBA::Boolean coolingon;
    CORBA::Long quality;
    CORBA::Double partial;
    std::string type;

    // preview:
    CORBA::Boolean preview_enabled;
    std::string preview_target;
};

Q_DECLARE_METATYPE(refreshdata)

class CorbaRefresher : public QObject
{
Q_OBJECT
public:
    CorbaRefresher(maci::SmartPtr<Control::OpticalTelescope> opt, QMutex * m);
public slots:
    void refresh();
    void startUpdateLoop();
signals:
    void resultReady(const struct refreshdata &);
    void error(const QString &);
private:
    maci::SmartPtr<Control::OpticalTelescope> opt;
    QMutex *m;
};



class ExposureWorker : public QObject
{
    Q_OBJECT
public:
    ExposureWorker(maci::SmartPtr<Control::OpticalTelescope> opt, QMutex *m);
    // void setSaveEnabled(bool enabled);

private slots:
    void update();
public slots:
    void startUpdateLoop();
    void initExposure();
    void abortExposure();
    void initTransfer();


signals:
    void exposureDone();
    void exposureProgress(int);
    void error(const QString&);
    void filesizeKnown(int);
    void transferDone();
    void transferProgress(int);

private:
    maci::SmartPtr<Control::OpticalTelescope> opt;
    QTimer * updateTimer;
    QElapsedTimer exposureTimer;
    QElapsedTimer transferTimer;
};


#endif // MAINWINDOW_H
