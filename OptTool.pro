#-------------------------------------------------
#
# Project created by QtCreator 2018-03-12T17:50:42
#
#-------------------------------------------------

QT       += core gui


TARGET = OptTool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h\
			main.h

FORMS    += mainwindow.ui


INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/ace
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/IORTable
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/IFR_Client
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/PortableServer
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/SmartProxies
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/DynamicAny
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/DynamicInterface
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/Messaging
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/tao/Valuetype
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/orbsvcs
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/TAO/orbsvcs/orbsvcs/Log
INCLUDEPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux
INCLUDEPATH += /home/almamgr/INTROOT/include
INCLUDEPATH += /alma/ACS-2017DEC/ACSSW/include
INCLUDEPATH += /alma/ACS-2017DEC/boost/include
INCLUDEPATH += /usr/X11R6/include/X11
INCLUDEPATH += /alma/ACS-2017DEC/ACSSW/include/cat
INCLUDEPATH += /alma/ACS-2017DEC/ACSSW/include/rtd
INCLUDEPATH += /alma/ACS-2017DEC/ACSSW/include
INCLUDEPATH += /alma/ACS-2017DEC/Python/include/python2.6
INCLUDEPATH += /alma/ACS-2017DEC/tcltk/include
INCLUDEPATH += /usr/include
INCLUDEPATH += /usr/local/include

QMAKE_RPATHDIR += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/lib
QMAKE_RPATHDIR += /home/almamgr/INTROOT/lib
QMAKE_RPATHDIR += /alma/ACS-2017DEC/ACSSW/lib
QMAKE_RPATHDIR += /alma/ACS-2017DEC/boost/lib
QMAKE_RPATHDIR += /usr/X11R6/lib
QMAKE_RPATHDIR += /alma/ACS-2017DEC/ACSSW/lib
QMAKE_RPATHDIR += /alma/ACS-2017DEC/Python/lib
QMAKE_RPATHDIR += /alma/ACS-2017DEC/tcltk/lib

LIBPATH += /alma/ACS-2017DEC/TAO/ACE_wrappers/build/linux/lib
LIBPATH += /home/almamgr/INTROOT/lib
LIBPATH += /alma/ACS-2017DEC/ACSSW/lib
LIBPATH += /alma/ACS-2017DEC/boost/lib
LIBPATH += /usr/X11R6/lib
LIBPATH += /alma/ACS-2017DEC/ACSSW/lib
LIBPATH += /alma/ACS-2017DEC/Python/lib
LIBPATH += /alma/ACS-2017DEC/tcltk/lib

LIBS += -lcontrolAlarmHelper
LIBS += -lmaciClient
LIBS += -lsla
LIBS += -lacstime
LIBS += -lTETimeUtil
LIBS += -lControlExceptions
LIBS += -lOpticalTelescopeBaseStubs
LIBS += -lOpticalTelescopeStubs
LIBS += -lOpticalTelescopeEth
LIBS += -lOpticalTelescopeImpl
