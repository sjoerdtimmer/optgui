#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "main.h"

#include <maciSimpleClient.h>
#include <loggingLoggingProxy.h>
#include <loggingLogSvcHandler.h>
#include <loggingMACROS.h>
#include <ControlBasicInterfacesC.h>
#include <EthernetDeviceInterface.h>

#include <acscomponentImpl.h>
#include "OpticalTelescopeBaseS.h"
#include "OpticalTelescopeBaseC.h"
#include "OpticalTelescopeS.h"
#include "OpticalTelescopeC.h"

#include "ControlAntennaInterfacesC.h"
#include <ControlCommonCallbacksC.h>
#include <AntennaC.h>
#include <ControlBasicInterfacesC.h>

#include <QDesktopServices>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDir>
#include <QMap>
#include <QMessageBox>

CorbaRefresher::CorbaRefresher(maci::SmartPtr<Control::OpticalTelescope> o, QMutex *_m) : m(_m)
{
    this->opt = o;
}

void CorbaRefresher::startUpdateLoop()
{
    QTimer * timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this, SLOT(refresh()));
    timer->start(200); // refresh takes more but while not connected this prevents a flood of checks (as compared to ->start(0))
}

void CorbaRefresher::refresh()
{
    QElapsedTimer timer;
    timer.start();
    // qDebug() << tr("refresh executed in thread %1").arg(QThread::currentThreadId());
    struct refreshdata result;
    result.optstate = Control::HardwareDevice::Undefined;
    try
    {
        result.optstate = opt->getHwState();
    }
    catch (CORBA::Exception &e)
    {
        emit error((tr("CORBA exception \"%1\" caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str())));
        return;
    }

    if (result.optstate==Control::HardwareDevice::Operational)
    {
        try
        {
            ACS::Time t;
            // in case of corba errors: set this to false so that -if something goes wrong- the ui does not attempt to interpret the data
            result.focuser_connected = false;
            result.sbig_connected = false;
            result.focuser_connected = opt->GET_FOCUSER_CONNECTED(t);
            result.sbig_connected    = opt->GET_SBIG_CONNECTED(t);

            if (result.focuser_connected)
            {
                result.ismoving   = opt->GET_FOCUSER_MOVING(t);
                result.position   = opt->GET_FOCUSER_POSITION(t);
                result.target     = opt->GET_FOCUSER_TARGET(t);
                result.ishalfstep = opt->GET_FOCUSER_HALFSTEP(t);
                result.temp       = opt->GET_FOCUSER_TEMP(t);
                result.speed      = opt->GET_FOCUSER_SPEED(t);
            }

            if (result.sbig_connected)
            {
                // qDebug() << "requesting exposure progress...";
                // result.exposureinprogress = ! opt->GET_EXPOSURE_DONE(t);
                // qDebug() << tr("attempting to lock mutex from thread %1").arg(QThread::currentThreadId());
                if (m->tryLock()) // just skip if not available
                // if (! result.exposureinprogress)
                // m->lock();
                { // during exposure, reading or setting the cooler will cause an error
                    // qDebug() << "mutex locked";
                    try
                    {
                        // qDebug() << "No exposure: continuing with sensitive ccd parameters";
                        result.actualtemp         = opt->GET_COOLER_MEASURE(t);
                        result.targettemp         = opt->GET_COOLER_SETPOINT(t);
                        result.coolingon          = opt->GET_SBIG_COOLER(t);
                        // qDebug() << "refresh of ccd params done";
                    }
                    catch (CORBA::Exception& e)
                    {
                        emit error(tr("CORBA exception \"%1\" caught inside mutex locked update.").arg(e._info().c_str()));
                    }
                    // qDebug() << "unlocking mutex";
                    m->unlock();
                }
                else
                {
                    qDebug() << "skipping temp refresh because exposure is in progress";
                }
            }

            // some things are not stored in the camera but in the SBC: we can fetch those even if the camera is not connected:
            result.quality         = opt->GET_SBIG_QUALITY(t);
            result.partial         = opt->GET_SBIG_PARTIAL(t);
            result.type            = opt->GET_SBIG_TYPE(t);
            result.exposuretime    = opt->GET_SBIG_EXPOSURE(t);
            result.preview_enabled = opt->GET_SBIG_PREVIEW_ENABLED(t);
            result.preview_target  = opt->GET_SBIG_PREVIEW_TARGET(t);

            int numerrors = opt->GET_RECENT_ERRORS(t);
            if (0<numerrors)
            {
                emit error(tr("%1 errors were retrieved from the SCPI error queue after executing the refresh. better check jlog...").arg(numerrors));
            }

        }
        catch(CORBA::Exception& e) {
            emit error(tr("CORBA exception \"%1\" caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
        }
        catch(...)
        {
            emit error(tr("A non-corba exception occured."));
        }
    }
    qDebug() << tr("CORBA Refresh took %1 ms").arg(QString::number(timer.elapsed()));
    emit resultReady(result);
}


ExposureWorker::ExposureWorker(maci::SmartPtr<Control::OpticalTelescope> o, QMutex *_m) : m(_m)
{
    this->opt = o;
    exposureTimer.invalidate(); // mark as not-waiting for exposure right now
}

void ExposureWorker::startUpdateLoop()
{
    updateTimer = new QTimer(this);
    connect(updateTimer, SIGNAL(timeout()),this, SLOT(update()));
    updateTimer->start(200);
}

void ExposureWorker::initExposure()
{
    // qDebug() << tr("initExposure runs from thread id %1").arg(QThread::currentThreadId());
    QElapsedTimer t;
    t.start();
    m->lock();// this blocks until it has the lock
    // qDebug() << "mutex locked";
    qDebug() << tr("starting a new exposure (had to wait %1ms for lock)").arg(QString::number(t.elapsed()));
    try
    {
        opt->SET_SBIG_EXPOSURE_START();
    }
    catch(CORBA::Exception& e) {
        emit error(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
    exposureTimer.start();

}

void ExposureWorker::abortExposure()
{
    try
    {
        opt->SET_SBIG_EXPOSURE_ABORT();
        // exposureTimer.invalidate();
    }
    catch(CORBA::Exception& e) {
        emit error(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void ExposureWorker::update()
{
    // qDebug() << tr("update is executed in thread ") << QThread::currentThreadId();
    if (exposureTimer.isValid())
    { // exposure was in progress
        // std::cout << "checking progress now...\n";
        try
        {
            ACS::Time t;
            CORBA::Boolean exposureFinished = opt->GET_EXPOSURE_DONE(t);
            if (exposureFinished)
            {
                exposureTimer.invalidate();
                // qDebug() << "exposure done";
                // qDebug() << "unlocking mutex";
                m->unlock();
                emit exposureDone();
            }
            else
            {
                qint64 elapsed = exposureTimer.elapsed();
                emit exposureProgress(elapsed);
            }
        }
        catch(CORBA::Exception& e) {
            emit error(tr("CORBA exception \"%1\" caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
        }
    }
}


void ExposureWorker::initTransfer()
{
    // qDebug() << tr("transfer is run from thread ") << QThread::currentThreadId();
    // now transfer the file
    ACS::Time t;
    int size = opt->GET_FILE_SIZE(t);
    if (size > 0) // 0 means the exposure was aborted
    {
        emit filesizeKnown(size);
        QByteArray rawfitsdata;
        rawfitsdata.reserve(size);

        // QFile b64file("/tmp/fits_base64.txt");
        // b64file.open(QIODevice::WriteOnly|QIODevice::Append);

        while(rawfitsdata.size() < size)
        {
            // qDebug() << tr("requesting chuck..");
            QByteArray b64data = QByteArray(opt->GET_FILE_PARTIAL(t));
            // b64file.write(b64data);
            // qDebug() << tr("got %1 bytes").arg(b64data.size());
            QByteArray rawdata = QByteArray::fromBase64(b64data);
            // qDebug() << tr("done recoding to %1 bytes").arg(rawdata.size());
            rawfitsdata.append(rawdata);
            // qDebug() << tr("done appending to master file");
            emit transferProgress(rawfitsdata.size());
        }

        QFile file("/tmp/fitsfile.fits");
        file.open(QIODevice::WriteOnly);
        file.write(rawfitsdata);
        file.close();
        // b64file.close();
        emit transferDone();
    }
}





MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // attempt to read saved ip
    QString cachefilename = QString("%1/sbighost.txt").arg(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
    // QString cachefilename = QStandardPaths::locate(QStandardPaths::AppDataLocation, QString("sbighostname.txt"));
    QFile cachefile(cachefilename);
    if (cachefile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // out << ui->lineEdit_sbig_dev->text();
        QTextStream in(&cachefile);
        ui->lineEdit_sbig_dev->setText(in.readAll());
    }

    previewPanel.setScaledContents(true);
}

MainWindow::~MainWindow()
{
    corbaRefreshThread.quit();
    exposureWorkerThread.quit();
    corbaRefreshThread.wait();
    exposureWorkerThread.wait();
    delete ui;
}

void MainWindow::setClient(maci::SimpleClient * c)
{
	client = c;
    // define comoponent specification:
    maci::ComponentSpec optspec;
    optspec.component_name = "CONTROL/DV01/OpticalTelescope";
    optspec.component_type = "IDL:alma/Control/OpticalTelescope:1.0";
    optspec.component_code = "*";
    optspec.container_name = "*";

    // get a smart pointer to a collocated component:
    opt = client->getContainerServices()->getCollocatedComponentSmartPtr<Control::OpticalTelescope>(optspec, false, "CONTROL/DV01");

    cout << "client configured" << endl;

    exposuremutex = new QMutex();
    // start a background thread that fetches updates:
    CorbaRefresher * refresher = new CorbaRefresher(opt, exposuremutex);
    refresher->moveToThread(&corbaRefreshThread);
    // cleanup:
    connect(&corbaRefreshThread, SIGNAL(started()), refresher, SLOT(startUpdateLoop()));
    connect(&corbaRefreshThread, SIGNAL(finished()), refresher, SLOT(deleteLater()));
    // wait for results:
    connect(refresher, SIGNAL(resultReady(const struct refreshdata&)), this, SLOT(updateCorbaDataInUI(const struct refreshdata&)));
    connect(refresher, SIGNAL(error(const QString&)), this, SLOT(on_refresh_error(const QString&)));
    // start the thread:
    corbaRefreshThread.start();


    // start background thread that monitors exposure progress
    exposureWorker = new ExposureWorker(opt, exposuremutex);
    exposureWorker->moveToThread(&exposureWorkerThread);

    connect(&exposureWorkerThread, SIGNAL(started()), exposureWorker, SLOT(startUpdateLoop()));
    connect(&exposureWorkerThread, SIGNAL(finished()), exposureWorker, SLOT(deleteLater()));
    connect(exposureWorker, SIGNAL(exposureDone()), this, SLOT(on_exposure_done()));
    connect(exposureWorker, SIGNAL(exposureProgress(const int)), this, SLOT(on_exposure_progress(const int)));
    connect(exposureWorker, SIGNAL(transferDone()), this, SLOT(on_transfer_done()));
    connect(exposureWorker, SIGNAL(transferProgress(const int)), this, SLOT(on_transfer_progress(const int)));
    connect(exposureWorker, SIGNAL(filesizeKnown(int)), this, SLOT(on_filesizeKnown(int)));
    connect(exposureWorker, SIGNAL(error(const QString&)), this, SLOT(on_refresh_error(const QString&)));

    exposureWorkerThread.start();

}

void MainWindow::on_exposure_done()
{
    // ui->plainText_logs->appendPlainText(tr("Exposure DONE!!!"));
    ui->progressBar_exposure->setValue(ui->progressBar_exposure->maximum());
    ui->label_exposure_status->setText("done");
    if (ui->groupBox_fitsoutput->isChecked())
    {
        ui->label_transfer_status->setText("starting...");
        QMetaObject::invokeMethod( exposureWorker, "initTransfer" );
        // exposureWorker->initTransfer();
    }
    else if (ui->checkBox_continuous_mode->checkState()==Qt::Checked)
    {
        startExposure();
    }
}

void MainWindow::on_exposure_progress(const int progress)
{
    // ui->plainText_logs->appendPlainText(QString("Exposure progress: %1").arg(progress));

    if (progress < ui->progressBar_exposure->maximum())
    {
        ui->progressBar_exposure->setValue(progress);
        ui->label_exposure_status->setText(QString("%1/%2").arg(progress).arg(ui->progressBar_exposure->maximum()));
    }
    else
    {
        ui->progressBar_exposure->setValue(ui->progressBar_exposure->maximum());
        ui->label_exposure_status->setText(QString("readout..."));
    }

}

void MainWindow::on_filesizeKnown(int size)
{
    ui->plainText_logs->appendPlainText(tr("file size is: %1 MB").arg(0.00000095367431640625*size, 0, 'f', 2));
    ui->progressBar_transfer->setMaximum(size);
}

void MainWindow::on_transfer_done()
{
    // ui->plainText_logs->appendPlainText(tr("transfer complete"));
    QFile file("/tmp/fitsfile.fits");
    QString newfilename = QString("%1%2.fits")
        .arg(ui->lineEdit_fits_basename->text())
        .arg(ui->spinBox_fits_number->value(),ui->spinBox_fits_padding->value() , 10, QLatin1Char('0'));
    if (!file.rename(newfilename))
    {
        ui->plainText_logs->appendPlainText(tr("failed to save that filename. Possibly it already exists?"));
    }
    ui->spinBox_fits_number->setValue(ui->spinBox_fits_number->value()+1);
    ui->label_transfer_status->setText("done");
    if (ui->checkBox_acs_preview->checkState() == Qt::Checked)
    {
        this->previewFitsFile(newfilename);
    }
    if (ui->checkBox_continuous_mode->checkState()==Qt::Checked)
    {
        startExposure();
    }
}

void MainWindow::previewFitsFile(QString filename)
{
    qDebug() << "attempting preview...";
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << tr("failed to open file for reading");
        return;
    }
    QMap<QString, QString> headers;
    bool done = false;
    while (!done)
    {
        QString line = QString(file.read(80));
        // qDebug() << tr("processing line: %1").arg(line);
        if (line.startsWith("END"))
            done = true;
        else
        {
            QStringList lineparts = line.split(QRegExp("[=/]"));
            if (lineparts.size() != 3) continue;
            // qDebug() << tr("processing line \"%1\" with %2 parts").arg(line).arg(lineparts.size());
            QString key =lineparts[0];
            QString value =lineparts[1];
            QString comment =lineparts[2];
            headers.insert(key.trimmed(), value.trimmed());
        }
    }
    if (!headers.contains("NAXIS1") || !headers.contains("NAXIS2")) return; // going out of scope should close the QFile...
    int width  = headers.value("NAXIS1").toInt();
    int height = headers.value("NAXIS2").toInt();
    int bitpix = headers.value("BITPIX", "16").toInt();
    if (bitpix != 8 && bitpix != 16 && bitpix != 32)
    {
        qDebug() << tr("At this moment only 8, 16 and 32 are acceptable values of bitpix. Skipping preview.");
        return;
    }
    if (width == 0 || height == 0) return;

    // fits headers are always multiples of 2880 bytes:
    size_t start_of_data = ((file.pos()+2880-1)/2880)*2880;
    file.seek(start_of_data);

    QElapsedTimer timer;
    timer.start();
    size_t rawsize = bitpix/8 * width * height;
    char * rawdata = (char*)malloc(rawsize);
    if (file.read(rawdata, rawsize) != rawsize)
    {
        qDebug() << tr("failed to read %1 raw bytes from fits file. Is the file corrupt?").arg(rawsize);
        free(rawdata);
        return;
    }
    // qDebug() << tr("reading data: %1 ms").arg(timer.restart());
    // determine min.max
    uint32_t minpixelvalue = 1 << bitpix - 1;
    uint32_t maxpixelvalue = 0;
    for (int i=0;i<height*width;i++)
    {
        switch (bitpix)
        {
            case 8:
            {
                uint8_t * pixeldata = (uint8_t *)rawdata;
                minpixelvalue = MIN(minpixelvalue, pixeldata[i]);
                maxpixelvalue = MAX(maxpixelvalue, pixeldata[i]);
            } break;
            case 16:
            {
                uint16_t * pixeldata = (uint16_t *)rawdata;
                minpixelvalue = MIN(minpixelvalue, pixeldata[i]);
                maxpixelvalue = MAX(maxpixelvalue, pixeldata[i]);
            } break;
            case 32:
            {
                uint32_t * pixeldata = (uint32_t *)rawdata;
                minpixelvalue = MIN(minpixelvalue, pixeldata[i]);
                maxpixelvalue = MAX(maxpixelvalue, pixeldata[i]);
            } break;
        }
    }
    // qDebug() << tr("pixel values range from %1 to %2").arg(minpixelvalue).arg(maxpixelvalue);
    // qDebug() << tr("min/max took %1 ms").arg(timer.restart());

    // create the image
    QImage image = QImage(width, height, QImage::Format_RGB32);
    // qDebug() << tr("empty image created: %1 ms").arg(timer.restart());
    uint32_t val;
    double sharpness = 0;
    uint32_t gradx, grady;
    for (int y=0;y<height;y++)
    for (int x=0;x<width;x++)
    {
        switch (bitpix)
        {
            case 8:
            {
                uint8_t * pixeldata = (uint8_t *)rawdata;
                val = pixeldata[x+width*y];
                if (x>0 && y>0)
                {
                    gradx = val - pixeldata[x-1+width*y];
                    grady = val - pixeldata[x+width*(y-1)];
                }
            } break;
            case 16:
            {
                uint16_t * pixeldata = (uint16_t *)rawdata;
                val = pixeldata[x+width*y];
                if (x>0 && y>0)
                {
                    gradx = val - pixeldata[x-1+width*y];
                    grady = val - pixeldata[x+width*(y-1)];
                }
            } break;
            case 32:
            {
                uint32_t * pixeldata = (uint32_t *)rawdata;
                val = pixeldata[x+width*y];
                if (x>0 && y>0)
                {
                    gradx = val - pixeldata[x-1+width*y];
                    grady = val - pixeldata[x+width*(y-1)];
                }
            } break;
        }
        if (x>0 && y>0)
        {
            sharpness += sqrt(gradx*gradx + grady*grady);
        }

        // put in range 0-255:
        val = 255 - (val - minpixelvalue) / ((maxpixelvalue - minpixelvalue)/255); // I divide by 255 in the denominator rather than multiply after the division to prevent overflow of uint32_t
        // qDebug() << tr("scaled pixel value: %1").arg(val);
        // qDebug() << tr("raw pixel data: 0x%1").arg(val | (val<<8) | (val<<16), 8, 16, QLatin1Char('0'));
        image.setPixel(x, y, val | (val<<8) | (val<<16)); // at << 24 there is opacity which we leave at 0
        // file.read(bitpix/8-1); // discard less significant bytes
    }
    sharpness /= (width-1)*(height-1);
    ui->label_sharpness->setText(QString("%1").arg(sharpness, 0, 'f', 3));
    // qDebug() << tr("image pixels filled: %1 ms").arg(timer.restart());

    QPixmap pix = QPixmap::fromImage(image);
    // qDebug() << tr("image to pixmap conversion: %1 ms").arg(timer.restart());

    previewPanel.setPixmap(pix);
    previewPanel.show();
    // qDebug() << tr("showing the label: %1 ms").arg(timer.restart());

    free(rawdata);
}

void MainWindow::on_checkBox_acs_preview_stateChanged(int state)
{
    if (state == Qt::Unchecked)
    {
        previewPanel.hide();
    }
}

void MainWindow::on_transfer_progress(const int progress)
{
    // ui->plainText_logs->appendPlainText(tr("transfer progress: %1 bytes").arg(progress));
    ui->progressBar_transfer->setValue(progress);
    ui->label_transfer_status->setText(QString("%1/%2").arg(0.000001*progress, 0, 'f', 2).arg(0.000001*ui->progressBar_transfer->maximum(), 0, 'f', 2));
}

void MainWindow::on_refresh_error(const QString &msg)
{
    ui->plainText_logs->appendPlainText(msg);
}

void MainWindow::updateCorbaDataInUI(const struct refreshdata & data)
{
    switch (data.optstate) {
        case Control::HardwareDevice::Undefined:    ui->label_hwStatus->setText("Undefined"); break;
        case Control::HardwareDevice::Start:        ui->label_hwStatus->setText("Start"); break;
        case Control::HardwareDevice::Configure:    ui->label_hwStatus->setText("Configure"); break;
        case Control::HardwareDevice::Initialize:   ui->label_hwStatus->setText("Initialize"); break;
        case Control::HardwareDevice::Operational:  ui->label_hwStatus->setText("Operational"); break;
        case Control::HardwareDevice::Diagnostic:   ui->label_hwStatus->setText("Diagnostic"); break;
        case Control::HardwareDevice::Stop:         ui->label_hwStatus->setText("Stop"); break;
        default:                                    ui->label_hwStatus->setText("Unknown"); break;
    }

    // qDebug() << "current hw state: "<<ui->label_hwStatus->text();



    if (data.optstate != Control::HardwareDevice::Operational)
    {
        // disable everything until component is connected
        ui->groupBox_sbc_connections->setEnabled(false);
        ui->groupBox_sbig->setEnabled(false);
        ui->groupBox_focuser->setEnabled(false);
        ui->groupBox_ds9_output->setEnabled(false);
        ui->groupBox_fitsoutput->setEnabled(false);
        ui->groupBox_fitsheaders->setEnabled(false);
        ui->groupBox_coordinates->setEnabled(false);
        ui->groupBox_main->setEnabled(false);
    }
    else
    {
        // enable whole groups:
        ui->groupBox_sbc_connections->setEnabled(true);
        ui->groupBox_sbig->setEnabled(data.sbig_connected);
        ui->groupBox_focuser->setEnabled(data.focuser_connected);
        ui->groupBox_ds9_output->setEnabled(data.sbig_connected);
        ui->groupBox_fitsoutput->setEnabled(data.sbig_connected);
        ui->groupBox_fitsheaders->setEnabled(data.sbig_connected);
        // coordinates intentionally not enabled until mount also implemented
        // main temporarily available but in reality also depends on mount:
        ui->groupBox_main->setEnabled(data.sbig_connected);


        // enable connect/disconnect buttons:
        // sbig connect is always available because it has auto-disconnect
        // so only the disconnect button toggles
        ui->pushButton_sbig_disconnect->setEnabled(data.sbig_connected);
        // ui->pushButton_sbig_reset->setEnabled(data.sbig_connected);
        ui->pushButton_focuser_connect->setEnabled(!data.focuser_connected);
        ui->pushButton_focuser_autoconnect->setEnabled(!data.focuser_connected);
        ui->pushButton_focuser_disconnect->setEnabled(data.focuser_connected);
        ui->lineEdit_focuser_dev->setEnabled(!data.focuser_connected);



        if (data.focuser_connected)
        {
            ui->checkBox_focuser_moving->setChecked(data.ismoving);
            if (!ui->checkBox_focuser_halfstepping->hasFocus())
                ui->checkBox_focuser_halfstepping->setChecked(data.ishalfstep);
            if (!ui->spinBox_focuser_target->hasFocus())
                ui->spinBox_focuser_target->setValue(data.target);
            ui->label_focuser_current->setText(tr("%1 steps").arg(data.position));
            ui->label_focuser_target->setText(tr("%1 steps").arg(data.target));
            ui->label_focuser_temp->setText(tr("%1 degrees").arg(0.5*data.temp));
            if (!ui->comboBox_focuser_speed->hasFocus())
            {
                switch (data.speed)
                {
                    case 16:  ui->comboBox_focuser_speed->setCurrentIndex(0); break;
                    case 32:  ui->comboBox_focuser_speed->setCurrentIndex(1); break;
                    case 63:  ui->comboBox_focuser_speed->setCurrentIndex(2); break;
                    case 125: ui->comboBox_focuser_speed->setCurrentIndex(3); break;
                    case 250: ui->comboBox_focuser_speed->setCurrentIndex(4); break;
                    default: ui->plainText_logs->appendPlainText(tr("received an invalid speed"));
                    // TODO: maybe the above should go through error?
                }
            }
        }

        if (data.sbig_connected)
        {
            if (!ui->doubleSpinBox_cooler_setpoint->hasFocus())
                ui->doubleSpinBox_cooler_setpoint->setValue(data.targettemp);
            ui->lineEdit_cooler_measure->setText(tr("%1 deg").arg(data.actualtemp));
            if (!ui->doubleSpinBox_exposure->hasFocus())
                ui->doubleSpinBox_exposure->setValue(data.exposuretime);
            if (!ui->checkBox_sbig_cooler->hasFocus())
                ui->checkBox_sbig_cooler->setChecked(data.coolingon);
            if (!ui->comboBox_sbig_quality->hasFocus())
                ui->comboBox_sbig_quality->setCurrentIndex(data.quality - 1); // SBC has 1-based quality
            if (!ui->doubleSpinBox_sbig_partial->hasFocus())
                ui->doubleSpinBox_sbig_partial->setValue(data.partial);
            // ui->progressBar_exposure->setEnabled(data.exposuredone);
            if (!ui->comboBox_sbig_type->hasFocus())
            {
                QString newtype = QString::fromStdString(data.type).trimmed();
                if (newtype.compare("LIGHTFRAME")==0)
                    ui->comboBox_sbig_type->setCurrentIndex(0);
                else if (newtype.compare("DARKFRAME")==0)
                    ui->comboBox_sbig_type->setCurrentIndex(1);
                else if (newtype.compare("AUTODARK")==0)
                    ui->comboBox_sbig_type->setCurrentIndex(2);
                else
                    ui->plainText_logs->appendPlainText(tr("unknown image type configured in sbc: %1").arg(newtype));
            }

            if (!ui->groupBox_ds9_output->hasFocus())
                ui->groupBox_ds9_output->setChecked(data.preview_enabled);
            QStringList targetparts = QString::fromStdString(data.preview_target).trimmed().split(":");
            if (!ui->lineEdit_ds9_host->hasFocus())
            {
                ui->lineEdit_ds9_host->setText(targetparts[0]);
            }
            if (!ui->spinBox_ds9_port->hasFocus() && targetparts.size()>1)
            {
                ui->spinBox_ds9_port->setValue(targetparts[1].toInt());
            }
        }
    }
}


void MainWindow::on_pushButton_hwStop_clicked()
{
    try
    {
        opt->hwStop();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception \"%1\" caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_hwStart_clicked()
{
    try
    {
        opt->hwStart();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception \"%1\" caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }

}

void MainWindow::on_pushButton_hwConfigure_clicked()
{
    try
    {
        opt->hwConfigure();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception \"%1\" caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_hwInitialize_clicked()
{
    try
    {
        opt->hwInitialize();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }

}

void MainWindow::on_pushButton_hwOperational_clicked()
{
    try
    {
        opt->hwOperational();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_focuser_connect_clicked()
{
    try
    {
        opt->SET_FOCUSER_CONNECT(ui->lineEdit_focuser_dev->text().toStdString().c_str());
        // ui->groupBox_focuser->setEnabled(true);
        // ui->lineEdit_focuser_dev->setEnabled(false);
        // ui->pushButton_focuser_connect->setEnabled(false);
        // ui->pushButton_focuser_autoconnect->setEnabled(false);
        // ui->pushButton_focuser_disconnect->setEnabled(true);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_focuser_autoconnect_clicked()
{
    try
    {
        opt->SET_FOCUSER_AUTOCONNECT();
        ACS::Time t;
        QString dev = QString(opt->GET_FOCUSER_DEVICE(t));
        ui->lineEdit_focuser_dev->setText(dev);
        // ui->groupBox_focuser->setEnabled(true);
        // ui->lineEdit_focuser_dev->setEnabled(false);
        // ui->pushButton_focuser_connect->setEnabled(false);
        // ui->pushButton_focuser_autoconnect->setEnabled(false);
        // ui->pushButton_focuser_disconnect->setEnabled(true);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }

}

void MainWindow::on_pushButton_focuser_disconnect_clicked()
{
    try
    {
        opt->SET_FOCUSER_DISCONNECT();
        // ui->groupBox_focuser->setEnabled(false);
        // ui->lineEdit_focuser_dev->setEnabled(true);
        // ui->pushButton_focuser_connect->setEnabled(true);
        // ui->pushButton_focuser_autoconnect->setEnabled(true);
        // ui->pushButton_focuser_disconnect->setEnabled(false);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_focuser_abort_clicked()
{
    try
    {
        opt->SET_FOCUSER_ABORT();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_focuser_current_set_clicked()
{
    try
    {
        opt->SET_FOCUSER_POSITION(ui->spinBox_focuser_current->value());
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_focuser_target_set_clicked()
{
    try
    {
        opt->SET_FOCUSER_TARGET(ui->spinBox_focuser_target->value());
        opt->SET_FOCUSER_GO();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }

}

void MainWindow::on_comboBox_focuser_speed_currentIndexChanged(int index)
{
    try
    {
        switch (ui->comboBox_focuser_speed->currentIndex())
        {
            case 0: opt->SET_FOCUSER_SPEED(16); break;
            case 1: opt->SET_FOCUSER_SPEED(32); break;
            case 2: opt->SET_FOCUSER_SPEED(63); break;
            case 3: opt->SET_FOCUSER_SPEED(125); break;
            case 4: opt->SET_FOCUSER_SPEED(250); break;
        }
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_checkBox_focuser_halfstepping_stateChanged(int state)
{
    try
    {
        opt->SET_FOCUSER_HALFSTEP(state==Qt::Checked);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_focuser_init_temp_clicked()
{
    try
    {
        opt->SET_FOCUSER_INIT_TEMP();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_sbig_connect_clicked()
{
    try
    {
        opt->SET_SBIG_CONNECT(ui->lineEdit_sbig_dev->text().toStdString().c_str());
        // ui->groupBox_sbig->setEnabled(true);
        // ui->lineEdit_sbig_dev->setEnabled(false);
        // ui->pushButton_sbig_connect->setEnabled(false);
        // ui->pushButton_sbig_disconnect->setEnabled(true);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }

    QString storagelocation = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
    QDir().mkpath(storagelocation);
    QString cachefilename = QString("%1/sbighost.txt").arg(storagelocation);
    // qDebug() << "saving to " << cachefilename;

    // QString cachefilename = QStandardPaths::locate(QStandardPaths::AppDataLocation, QString("sbighostname.txt"));
    QFile cachefile(cachefilename);
    if (cachefile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&cachefile);
        out << ui->lineEdit_sbig_dev->text();
    }
    else
    {
        qDebug() << "failed to open file to store ui prefilled values";
    }
}

void MainWindow::on_pushButton_sbig_reset_clicked()
{
    QMessageBox msgBox;
    msgBox.setText("Do you whish to send a *RESET?");
    msgBox.setInformativeText("This should only be necessary in rare cases (e.g. when the usb cable from the camera was detached during operation).");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    if( msgBox.exec() == QMessageBox::Yes )
    {
        try
        {
            opt->SET_SBIG_RESET();
        }
        catch(CORBA::Exception& e) {
            ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
        }
    }
}

void MainWindow::on_pushButton_sbig_disconnect_clicked()
{
    try
    {
        opt->SET_SBIG_DISCONNECT();
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_checkBox_sbig_cooler_stateChanged(int state)
{
    try
    {
        opt->SET_SBIG_COOLER_ENABLED(state==Qt::Checked);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_doubleSpinBox_cooler_setpoint_valueChanged(double val)
{
    try
    {
        opt->SET_COOLER_SETPOINT(val);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_doubleSpinBox_exposure_valueChanged(double val)
{
    try
    {
        opt->SET_SBIG_EXPOSURE(val);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_doubleSpinBox_sbig_partial_valueChanged(double val)
{
    try
    {
        opt->SET_SBIG_PARTIAL(val);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_comboBox_sbig_quality_currentIndexChanged(int index)
{
    try
    {
        opt->SET_SBIG_QUALITY(1 + ui->comboBox_sbig_quality->currentIndex()); // index 1-based in SBC
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_comboBox_sbig_type_currentIndexChanged(int index)
{
    try
    {
        switch(ui->comboBox_sbig_type->currentIndex())
        {
            case 0:opt->SET_SBIG_TYPE("LIGHTFRAME"); break;
            case 1:opt->SET_SBIG_TYPE("DARKFRAME"); break;
            case 2:opt->SET_SBIG_TYPE("AUTODARK"); break;
        }
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_focuser_autohome_clicked()
{
    try
    {
        opt->SET_FOCUSER_POSITION(ui->spinBox_focuser_homerange->value()); // index 1-based in SBC
        opt->SET_FOCUSER_TARGET(0);
        opt->SET_FOCUSER_GO();

    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_pushButton_clearlog_clicked()
{
    ui->plainText_logs->clear();
}

void MainWindow::on_pushButton_exposure_start_clicked()
{
    startExposure();
}

void MainWindow::startExposure()
{
    ui->label_exposure_status->setText("starting...");
    double exp = ui->doubleSpinBox_exposure->value();
    double multiplier = ui->comboBox_sbig_type->currentIndex()==2 ? 2 : 1; // *2 for Lightframe + darkframe
    ui->progressBar_exposure->setMaximum(exp*multiplier*1000); // *1000 because timer reports ms
    // qDebug() << tr("initExposure called from thread id %1").arg(QThread::currentThreadId());

    // exposureWorker->initExposure(exposuremutex);
    QMetaObject::invokeMethod( exposureWorker, "initExposure");
}

void MainWindow::on_pushButton_exposure_abort_clicked()
{
    exposureWorker->abortExposure();
}

void MainWindow::on_groupBox_ds9_output_clicked(bool checked)
{   // not triggered when the change is programmatic
    try
    {
        opt->SET_SBIG_PREVIEW_ENABLED(checked);
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_lineEdit_ds9_host_editingFinished()
{
    try
    {
        opt->SET_SBIG_PREVIEW_TARGET(QString("%1:%2").arg(ui->lineEdit_ds9_host->text()).arg(ui->spinBox_ds9_port->value()).toLocal8Bit().constData());
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}

void MainWindow::on_spinBox_ds9_port_editingFinished()
{
    try
    {
        opt->SET_SBIG_PREVIEW_TARGET(QString("%1:%2").arg(ui->lineEdit_ds9_host->text()).arg(ui->spinBox_ds9_port->value()).toLocal8Bit().constData());
    }
    catch(CORBA::Exception& e) {
        ui->plainText_logs->appendPlainText(tr("CORBA exception %1 caught. Are you sure the Single board computer is running and reachable?").arg(e._info().c_str()));
    }
}
