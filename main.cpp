#include "main.h"
#include "mainwindow.h"

#include <maciSimpleClient.h>

#include <QtGui/QApplication>
#include <QEvent>
#include <QDebug>



maci::SimpleClient client;

// class MyApplication Q_DECL_FINAL : public QApplication
// {
//     Q_OBJECT
// public:
//     MyApplication(int &argc, char **argv) : QApplication(argc, argv) {}
//
//     bool notify(QObject* receiver, QEvent* event) Q_DECL_OVERRIDE
//     {
//         try {
//             return QApplication::notify(receiver, event);
//         } catch (CORBA::Exception &e) {
//             // Handle the desired exception type
//             std::cout << "corba exception in notify" << std::endl;
//         } catch (...) {
//             // Handle the rest
//             std::cout << "other exception in notify" << std::endl;
//         }
//          return false;
//      }
// };
//
// #include "main.moc"

int main(int argc, char *argv[])
{
    // necessary to pass objects of this struct type around in qt signal/slots
    qRegisterMetaType<refreshdata>("refreshdata");

    QApplication a(argc, argv);
    MainWindow w;

    client.init(argc, argv);
    client.login();

    LOG_TO_OPERATOR(LM_ERROR, "Qt project maci client logged in!");

    w.setClient(&client);
    w.show();




    return a.exec();

}
